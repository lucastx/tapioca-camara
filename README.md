# Tapioca camara

## Installation
```
pip install tapioca-camara
```

## Documentation
``` python
from tapioca_camara import Camara


api = Camara()

```

No more documentation needed.

- Learn how Tapioca works [here](http://tapioca-wrapper.readthedocs.org/en/stable/quickstart.html)
- Explore this package using iPython
- Have fun!
